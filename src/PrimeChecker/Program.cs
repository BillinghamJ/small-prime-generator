﻿using System;
using System.Numerics;

namespace PrimeChecker
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("C# Prime Checker");
			Console.WriteLine("Copyright (C) James Billingham 2011. All rights reserved.");
			Console.WriteLine();

			while (true)
			{
				bool validInt = false;
				BigInteger prime = 0;
				
				Console.Write("Enter number to check: ");

				string seedInput = Console.ReadLine();

				validInt = BigInteger.TryParse(seedInput, out prime);

				if (validInt && prime.Sign != 1) { validInt = false; }

				if (!validInt) { Console.WriteLine("Not a valid integer."); }
				else
				{
					Console.WriteLine(prime * new BigInteger(Math.Log(2)) / new BigInteger(Math.Log(10)) + 1);
					Console.WriteLine("That number is " + (isPrime(prime) ? "" : "not ") + "prime.");
				}

				Console.WriteLine();
			}
		}

		public static bool isPrime(BigInteger number)
		{
			BigInteger s = new BigInteger();
			BigInteger t = number - 1;
			BigInteger b = new BigInteger(2);
			BigInteger nmin1 = number - 1;
			BigInteger r, j, smin1;

			if (number == 1)
				return false;
			else if (number == 2)
				return true;
			else if (number == 3)
				return true;

			while (t % 2 == 0)
			{
				t /= 2;
				s++;
			}

			smin1 = s - 1;

			for (int i = 0; i < 300; i++)
			{
				r = BigInteger.ModPow(b, t, number);

				if ((r != 1) && (r != nmin1))
				{
					j = new BigInteger(1);
					while ((j <= smin1) && (r != nmin1))
					{
						r = (r * r) % number;
						if (r == 1)
							return false;
						j++;
					}
					if (r != nmin1)
						return false;
				}

				if (b == 2)
					b++;
				else
					b += 2;
			}

			return true;
		}
	}
}
