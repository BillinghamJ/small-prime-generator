﻿using System;
using System.Numerics;

namespace PrimeGen
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("C# Prime Generator");
			Console.WriteLine("Copyright (C) James Billingham 2011. All rights reserved.");
			Console.WriteLine();

			bool validInt = false;
			BigInteger seed = 0;

			while (!validInt)
			{
				Console.Write("Enter seed Mersenne prime: ");
				string seedInput = Console.ReadLine();

				validInt = BigInteger.TryParse(seedInput, out seed);

				if (validInt && seed.Sign != 1) { validInt = false; }
				
				if (!validInt) { Console.WriteLine("Not a valid integer."); }

				Console.WriteLine();
			}

			Console.WriteLine("Starting prime creation.");
			Console.WriteLine("Initial Mersenne prime seed: " + seed.ToString());

			BigInteger currentNum;

			for (int j = 0; j < 5; j++)
			{
				currentNum = 1;

				for (BigInteger i = 0; i < seed; i++)
				{
					currentNum *= 2;
				}

				seed = currentNum;
				seed -= 1;

				Console.WriteLine("Generated Mersenne prime: " + seed.ToString());
			}

			while (true) { Console.ReadLine(); }
		}
	}
}
